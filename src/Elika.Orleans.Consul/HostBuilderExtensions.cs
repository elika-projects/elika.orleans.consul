﻿using System.Net;
using Consul;
using Elika.Hosting.Consul;
using Elika.Hosting.Consul.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Orleans.Configuration;
using Orleans.Hosting;

namespace Elika.Orleans.Consul;

/// <summary>
///     Extensions for <see cref="IHostBuilder"/>
/// </summary>
public static class HostBuilderExtensions
{
    /// <inheritdoc cref="GenericHostExtensions.UseOrleans"/>
    public static IHostBuilder UseOrleansWithConsul(
            this IHostBuilder hostBuilder,
            Action<HostBuilderContext, ISiloBuilder>? configureDelegate = null)
    {
        ArgumentNullException.ThrowIfNull(hostBuilder);
        
        return hostBuilder
            .UseConsul()
            .ConfigureServices((host, services) =>
            {
                services.AddOptions<ClusterOptions>()
                    .Bind(host.Configuration.GetSection(OptionsKeys.ClusterOptionsKey))
                    .Configure<IHostEnvironment>((options, env) =>
                    {
                        options.ServiceId = env.ApplicationName;
                    });

                services.AddOptions<EndpointOptions>()
                    .Bind(host.Configuration.GetSection(OptionsKeys.EndpointOptionsKey));

                services.AddOptions<ConsulClientOptions>()
                    .Configure<IOptions<ClusterOptions>>((options, cluster) =>
                    {
                        options.KVApp = cluster.Value.ServiceId;
                    });

                services.AddOptions<ConsulAgentOptions>()
                    .PostConfigure<IOptions<EndpointOptions>, IOptions<ClusterOptions>>((options, ep, cl) =>
                    {
                        var endpoint = ep.Value;
                        var cluster = cl.Value;

                        var instanceId = options.IsInDockerContainer
                            ? Environment.MachineName
                            : Math.Abs(DateTime.Now.GetHashCode()).ToString();
                        
                        options.AppId = $"{cluster.ClusterId}.{cluster.ServiceId}/{instanceId}";

                        options.Address = endpoint.AdvertisedIPAddress;
                        options.Port = endpoint.SiloPort;
                        options.Tags.Add($"{Tag.Cluster}:{cluster.ClusterId}");
                        options.Tags.Add($"{Tag.Service}:{cluster.ServiceId}");
                        options.Tags.Add($"{Tag.InstanceId}:{instanceId}");
                        options.Tags.Add($"{Tag.Type}:service");

                        options.Meta.Add(Meta.OrleansAdvertisedAddress, endpoint.AdvertisedIPAddress.ToString());

                        options.Meta.Add(Meta.OrleansSiloEndpoint,
                            endpoint.SiloListeningEndpoint is not null
                                ? endpoint.SiloListeningEndpoint.ToString()
                                : new IPEndPoint(endpoint.AdvertisedIPAddress, endpoint.SiloPort).ToString());

                        options.Meta.Add(Meta.OrleansGatewayEndpoint,
                            endpoint.GatewayListeningEndpoint is not null
                                ? endpoint.GatewayListeningEndpoint.ToString()
                                : new IPEndPoint(endpoint.AdvertisedIPAddress, endpoint.GatewayPort).ToString());
                    });

                services.AddOptions<ConsulHealthCheckOptions>()
                    .Configure<IOptions<EndpointOptions>>((options, ep) =>
                    {
                        options.Address ??= ep.Value.AdvertisedIPAddress;
                    });
            })
            .UseOrleans((host, siloBuilder) =>
            {
                siloBuilder.UseConsulSiloClustering(builder =>
                {
                    builder.Configure<IOptions<ConsulClientOptions>>((options, consul) =>
                    {                
                        options.ConfigureConsulClient(() => new ConsulClient(consul.Value));
                    });
                });

                configureDelegate?.Invoke(host, siloBuilder);
            });
    }

    /// <inheritdoc cref="OrleansClientGenericHostExtensions.UseOrleansClient"/>
    public static IHostBuilder UseOrleansClientWithConsul(this IHostBuilder builder, Action<HostBuilderContext, IClientBuilder>? configureDelegate = null)
    {
        return builder
            .UseConsul()
            .ConfigureServices((host, services) =>
            {
                services.AddOptions<ConsulAgentOptions>()
                    .PostConfigure((options) =>
                    {
                        var instanceId = options.IsInDockerContainer
                            ? Environment.MachineName
                            : Math.Abs(DateTime.Now.GetHashCode()).ToString();

                        options.Tags.Add($"{Tag.InstanceId}:{instanceId}");
                        options.Tags.Add($"{Tag.Type}:client");
                    });
            })
            .UseOrleansClient((host, cb) =>
            {
                cb.UseConsulClientClustering(ob =>
                    ob.Configure<IOptions<ConsulClientOptions>>((options, client) =>
                    {
                        var val = client.Value;
                        options.ConfigureConsulClient(() => new ConsulClient(val));
                    }));

                configureDelegate?.Invoke(host, cb);
            });
    }
}