﻿using Orleans.Configuration;

namespace Elika.Orleans.Consul
{
    /// <summary>
    ///     Options Keys in configurations
    /// </summary>
    public class OptionsKeys
    {
        /// <summary>
        ///     <see cref="ClusterOptions"/> key
        /// </summary>
        public const string ClusterOptionsKey = "Orleans:Cluster";

        /// <summary>
        ///     <see cref="EndpointOptions"/> key
        /// </summary>
        public const string EndpointOptionsKey = "Orleans:Endpoint";
    }
}
