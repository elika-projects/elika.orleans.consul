﻿namespace Elika.Orleans.Consul
{
    /// <summary>
    ///     Meta keys
    /// </summary>
    public class Meta
    {
        /// <summary>
        ///  Advertised Address in Orleans key
        /// </summary>
        public const string OrleansAdvertisedAddress = "orleans_advertised_address";

        /// <summary>
        ///     Silo Endpoint in Orleans key
        /// </summary>
        public const string OrleansSiloEndpoint = "orleans_silo_endpoint";

        /// <summary>
        ///     Gateway Endpoint in Orleans key
        /// </summary>
        public const string OrleansGatewayEndpoint = "orleans_gateway_endpoint";
    }
}
