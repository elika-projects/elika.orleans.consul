﻿namespace Elika.Orleans.Consul
{
    /// <summary>
    ///     Tag keys
    /// </summary>
    public class Tag
    {
        /// <summary>
        ///     Cluster name in Orleans tag key
        /// </summary>
        public const string Cluster = "cluster";

        /// <summary>
        ///     Service in Orleans tag key
        /// </summary>
        public const string Service = "service";

        /// <summary>
        ///     Instance Id tag key
        /// </summary>
        public const string InstanceId = "instance_id";

        /// <summary>
        ///     Type tag key
        /// </summary>
        public const string Type = "type";
    }
}
