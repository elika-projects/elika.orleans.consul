using Elika.Orleans.Consul;
using ExampleApp;

IHost host = Host.CreateDefaultBuilder(args)
    .UseOrleansWithConsul()
    .ConfigureServices(services =>
    {
        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();
